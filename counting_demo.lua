require 'torch'
require 'cutorch'
require 'cunn'
require 'loadcaffe'
require 'image'

local matio = require 'matio'
local utils = require 'utils'
local json = require 'json'


cmd = torch.CmdLine()
cmd:text('Options for getting counts from model')
cmd:text('-proto_file','/home/prithv1/compositional_counting/models/vgg16_c/deploy.prototxt')
cmd:text('-model_file','/home/prithv1/compositional_counting/models/vgg16_c/vgg16.caffemodel')
cmd:text('-img_path','temp')
cmd:text('-gpu',1)
cmd:text('-glancing_model','/home/prithv1/contextual_counting/torch/no_context/test_coco_nf_1hl_250_rmsprop_rel_1_lr_0.0001/no_context_count_best.t7')
cmd:text('-seed',123)
cmd:text('-backend','nn')

opt = cmd:parse(arg)

torch.setdefaulttensortype('torch.DoubleTensor')
torch.manualSeed(opt.seed)

------------------------------------------------------------------------------------------------------------------
------------------------------------Category Names----------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
cat_names = {}
temp = matio.load('coco_cat_names.mat')
for i=1,#temp['cat_names'] do
  table.insert(cat_names, tostring(temp['cat_names'][i]))
end
------------------------------------------------------------------------------------------------------------------
-------------------------------------------Loading Model----------------------------------------------------------
------------------------------------------------------------------------------------------------------------------
model = nn.Sequential()
cnn = loadcaffe.load(opt.proto_file, opt.model_file, opt.backend)
cnn_layers = 37

for x,y in pairs(cnn.modules) do
	if x <= cnn_layers then
		model:add(y)
	end
end

utils.caffe_to_torch(model)

mlp = torch.load(opt.glancing_model)

for x,y in pairs(mlp.modules) do
	model:add(y)
end

if opt.gpu > 0 then
	require 'cutorch'
	require 'cunn'
	model = model:cuda()
end

---------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------Image loading and preprocessing---------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------

local function preproc(img)
  if img:size(1) == 1 then
    img = torch.repeatTensor(img, 3, 1, 1)
  end
  img = image.scale(img, im_in, im_in)
  local mean_pixel = torch.Tensor({103.939, 116.779, 123.68})
  if opt.gpu > 0 then
    img = img:cuda()
    mean_pixel = mean_pixel:cuda()
  end
  local perm = torch.LongTensor{3, 2, 1}
  img = img:index(1, perm):mul(255.0)
  mean_pixel = mean_pixel:view(3, 1, 1):expandAs(img)
  img:add(-1, mean_pixel)
  return img
end

local img = image.load(opt.img_path)
img = preproc(img)

---------------------------------------------------------------------------------------------------------------------------------
---------------------------------------Getting counts----------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------

local counts = model:forward(img)
counts = counts:cmax(0)

---------------------------------------------------------------------------------------------------------------------------------
--------------------------------------Saving category-counts in a json file------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------
cat_count = {}
for i=1,#temp['cat_names'] do
	table.insert(cat_count,{category = cat_names[i], count = counts[i]})
end

local file = io.open('category_counts.json', 'w')
if file then
	local contents = json.encode(cat_count)
	file:write(contents)
	io.close(file)
end
	
---------------------------------------------------------------------------------------------------------------------------------



