require 'torch'
require 'loadcaffe'
require 'image'
require 'nn'

local matio = require 'matio'
local utils = require 'utils'
local json = require 'json'

local TorchModel = torch.class('CountingTorchModel')

function TorchModel:__init(proto_file, model_file, glancing_model, backend, seed, gpuid)

	self.proto_file = proto_file
	self.model_file = model_file
	self.glancing_model = glancing_model
	self.backend = backend
	self.seed = seed
	self.gpuid = gpuid
	self:prepareModel(proto_file, model_file, backend)
	torch.manualSeed(self.seed)
	torch.setdefaulttensortype('torch.DoubleTensor')

	if gpuid >= 0 then
		require 'cunn'
		require 'cutorch'
		cutorch.setDevice(gpuid + 1)
		cutorch.manualSeed(seed)
	end
	
end

function TorchModel:prepareModel(proto_file, model_file, glancing_model, backend)
	self.net = loadcaffe.load(proto_file, model_file, backend)
	local cnn = self.net
	local cnn_layers = 37
	for x,y in pairs(cnn.modules) do
		if x > cnn_layers then
			cnn:remove(x)
		end
	end
	utils.caffe_to_torch(cnn)
	local mlp = torch.load(glancing_model)
	for x,y in pairs(mlp.modules) do
		cnn:add(y)
	end
	cnn:evaluate()
end


function TorchModel:predict_count(img_path, im_in)

	local img = image.load(img_path)	
  if img:size(1) == 1 then
    img = torch.repeatTensor(img, 3, 1, 1)
  end
  img = image.scale(img, im_in, im_in)
  local mean_pixel = torch.Tensor({103.939, 116.779, 123.68})
  if opt.gpu > 0 then
    img = img:cuda()
    mean_pixel = mean_pixel:cuda()
  end
  local perm = torch.LongTensor{3, 2, 1}
  img = img:index(1, perm):mul(255.0)
  mean_pixel = mean_pixel:view(3, 1, 1):expandAs(img)
  img:add(-1, mean_pixel)

	if self.gpuid >= 0 then
		self.net:cuda()
		img = img:cuda()
	end

	local counts = self.net:forward(img)
	counts = counts:cmax(0)

	cat_names = {}
	temp = matio.load('coco_cat_names.mat')
	for i=1,#temp['cat_names'] do
	  table.insert(cat_names, tostring(temp['cat_names'][i]))
	end

	local cat_count = {}
	for i=1,#temp['cat_names'] do
		table.insert(cat_count,{category = cat_names[i], count = counts[i]})
	end

	local file = io.open('category_counts.json', 'w')
	if file then
	  local contents = json.encode(cat_count)
	  file:write(contents)
	  io.close(file)
	end
	return contents
end

